package com.example.chetan.dynamiclayoutdemo;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;


public class DisplayAdvertismentss extends ActionBarActivity {

    LinearLayout addsContainer=null;
    Intent i;

    ArrayList<CreateAddDetails> addDetailsObj = new ArrayList<CreateAddDetails>();

    private ArrayList<AddBanner> bannerRow = new ArrayList<AddBanner>();
    private ArrayList<BannerDiamention> bannerDimentionArray = null; //new ArrayList<BannerDiamention>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_advertismentss);

        addsContainer = (LinearLayout)findViewById(R.id.addContainLayout);

        addDetailsObj = getIntent().getParcelableArrayListExtra("AddDetails");

        AdvertismentResponse response = new AdvertismentResponse();


        bannerRow.clear();

        for (int i =0;i<addDetailsObj.size();i++)
        {
            CreateAddDetails details = addDetailsObj.get(i);
            
            int hight = Integer.parseInt(details.getHight());
            int column = Integer.parseInt(details.getColumn());

            AddBanner banner = new AddBanner();
            banner.setHeight(String.valueOf(hight));
            banner.setNumber(String.valueOf(i));

            bannerDimentionArray = new ArrayList<BannerDiamention>();
            bannerDimentionArray.clear();

            for (int j=0;j<column;j++)
           {

               BannerDiamention bannerDiamentionObj = new BannerDiamention();
               bannerDiamentionObj.setImgUrl("");
               bannerDiamentionObj.setUrl("");


               bannerDimentionArray.add(bannerDiamentionObj);
           }

            banner.setBanners(bannerDimentionArray);

            bannerRow.add(banner);
            banner =null;

        }

        response.setBannerRow(bannerRow);

        System.out.println("Advertisments object " + response.toString());


        displayAdvertisment(response);


    }


    private void displayAdvertisment(AdvertismentResponse response) {

        ArrayList<AddBanner> displaybannerRow = new ArrayList<AddBanner>();
        AddBanner firstBanner = null;

        int rowSize,bannerHight,number;

        if(response!=null)
        {
            displaybannerRow = response.getBannerRow();
            rowSize = displaybannerRow.size();

            System.out.println("Advertisments row Size " + rowSize);

            for(int i =0 ;i<displaybannerRow.size();i++)
            {
                ArrayList<BannerDiamention> displayBanners = new ArrayList<BannerDiamention>();

                firstBanner = displaybannerRow.get(i);
                displayBanners = firstBanner.getBanners();
                bannerHight = Integer.parseInt(firstBanner.getHeight());
                number = Integer.parseInt(firstBanner.getNumber());

                System.out.println("Advertisments Display banner Size " + displayBanners.size());


                if (displayBanners.size()==1)
                {
                    ImageView image = new ImageView(DisplayAdvertismentss.this);

                    LinearLayout.LayoutParams imageParam1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,bannerHight);
                    // imageParam1.weight = 0.3f;

                    image.setPadding(5, 5, 5, 5);
                    image.setLayoutParams(imageParam1);
                    Bitmap Icon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.background);
                    image.setImageBitmap(Icon);
                    image.setScaleType(ImageView.ScaleType.FIT_XY);
                    image.setMaxHeight(bannerHight);


                    Animation myFadeInAnimation = AnimationUtils.loadAnimation(DisplayAdvertismentss.this, R.anim.tween);
                    image.startAnimation(myFadeInAnimation);


                   // image.startAnimation(animation);


                    addsContainer.addView(image);

                }else
                {
                    LinearLayout twoPArtLayout = new LinearLayout(DisplayAdvertismentss.this);
                    twoPArtLayout.setOrientation(LinearLayout.HORIZONTAL);
                    twoPArtLayout.setWeightSum(displayBanners.size());

                    for (int j=0;j<displayBanners.size();j++)
                    {
                        ImageView image = new ImageView(DisplayAdvertismentss.this);

                        LinearLayout.LayoutParams imageParam1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,bannerHight);
                        imageParam1.weight = 1f;

                        image.setLayoutParams(imageParam1);
                        Bitmap Icon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.background);
                        image.setImageBitmap(Icon);
                        image.setScaleType(ImageView.ScaleType.FIT_XY);
                        image.setPadding(5, 5, 5, 5);
                        image.setMaxHeight(bannerHight);
                        twoPArtLayout.addView(image);




                    }

                    addsContainer.addView(twoPArtLayout);

                }


            }


        }


    }


}
