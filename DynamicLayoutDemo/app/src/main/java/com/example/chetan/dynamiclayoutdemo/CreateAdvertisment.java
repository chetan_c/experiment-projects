package com.example.chetan.dynamiclayoutdemo;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class CreateAdvertisment extends ActionBarActivity {

    Button createAdd,displayAdd;
    LinearLayout addContainer;
    int cnt=0;

    List<EditText> allEdsHight = new ArrayList<EditText>();
    List<EditText> allEdsColumn = new ArrayList<EditText>();


    ArrayList<CreateAddDetails> addDetailsObj = new ArrayList<CreateAddDetails>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_advertisment);

        addDetailsObj.clear();

        createAdd = (Button)findViewById(R.id.create);
        displayAdd = (Button)findViewById(R.id.display);
        addContainer = (LinearLayout)findViewById(R.id.addContainLayout);

        createAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                createAdvertisment();
            }
        });

        displayAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addDetailsObj.clear();

                for(int i=0; i < allEdsHight.size(); i++){

                    CreateAddDetails obj = new CreateAddDetails();

                    obj.setHight(allEdsHight.get(i).getText().toString());
                    obj.setColumn(allEdsColumn.get(i).getText().toString());

                    addDetailsObj.add(obj);
                }


                for (int j =0;j<addDetailsObj.size();j++)
                {
                    CreateAddDetails obj = addDetailsObj.get(j);
                    System.out.println("Chk : Add" + " " + j + 1 + " Hight : " + obj.getHight() +" Column : "+obj.getColumn());
                }

                Intent i = new Intent(getApplicationContext(),DisplayAdvertismentss.class);
                i.putParcelableArrayListExtra("AddDetails", addDetailsObj);
                startActivity(i);


            }
        });

    }

    private void createAdvertisment() {

        cnt++;

        LinearLayout linear = new LinearLayout(CreateAdvertisment.this);
        linear.setOrientation(LinearLayout.HORIZONTAL);
        linear.setWeightSum(3);


        LinearLayout.LayoutParams layouyParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        layouyParam.weight = 1f;


        TextView tx = new TextView(CreateAdvertisment.this);
        tx.setText("Add " +" "+cnt);
        tx.setLayoutParams(layouyParam);

        EditText ed = new EditText(CreateAdvertisment.this);
        allEdsHight.add(ed);
        ed.setId(cnt);
        ed.setHint("Hight");
        ed.setInputType(InputType.TYPE_CLASS_NUMBER);
        ed.setLayoutParams(layouyParam);



        EditText ed2 = new EditText(CreateAdvertisment.this);
        allEdsColumn.add(ed2);
        ed2.setId(cnt);
        ed2.setHint("Column");
        ed2.setInputType(InputType.TYPE_CLASS_NUMBER);
        ed2.setLayoutParams(layouyParam);

        linear.addView(tx);
        linear.addView(ed);
        linear.addView(ed2);

        addContainer.addView(linear);

    }


}
