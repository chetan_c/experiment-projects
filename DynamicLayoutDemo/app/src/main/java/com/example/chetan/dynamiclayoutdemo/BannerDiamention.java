package com.example.chetan.dynamiclayoutdemo;

/**
 * Created by chetan on 18/8/15.
 */
public class BannerDiamention {

    private String col;
    private String colspan;
    private String url;
    private String imgUrl;


    public String getCol() {
        return col;
    }

    public void setCol(String col) {
        this.col = col;
    }

    public String getColspan() {
        return colspan;
    }

    public void setColspan(String colspan) {
        this.colspan = colspan;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
