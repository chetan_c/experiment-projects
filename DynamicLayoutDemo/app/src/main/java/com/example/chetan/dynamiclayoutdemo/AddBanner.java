package com.example.chetan.dynamiclayoutdemo;

import java.util.ArrayList;

/**
 * Created by chetan on 17/8/15.
 */
public class AddBanner {

    private ArrayList<BannerDiamention> banners = new ArrayList<BannerDiamention>();
    private String number;
    private String height;


    public ArrayList<BannerDiamention> getBanners() {
        return banners;
    }

    public void setBanners(ArrayList<BannerDiamention> banners) {
        this.banners = banners;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

  /*  public class BannerDiamention
    {
        private String col;
        private String colspan;
        private String url;
        private String imgUrl;

        public String getCol() {
            return col;
        }

        public void setCol(String col) {
            this.col = col;
        }

        public String getColspan() {
            return colspan;
        }

        public void setColspan(String colspan) {
            this.colspan = colspan;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }
    }
*/
}

