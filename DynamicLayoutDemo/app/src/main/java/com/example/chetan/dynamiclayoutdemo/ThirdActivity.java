package com.example.chetan.dynamiclayoutdemo;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;


public class ThirdActivity extends ActionBarActivity {


    ArrayList<Banner> addArrayList= new ArrayList<Banner>();

    Button next;
    LinearLayout addContaintLayout=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        next = (Button)findViewById(R.id.next);
        addContaintLayout = (LinearLayout)findViewById(R.id.addContainLayout);


        Banner add1 = new Banner();
        add1.setPosition(1);
        add1.setSpam(0);
        add1.setImage(R.mipmap.ic_launcher);

        Banner add2 = new Banner();
        add1.setPosition(2);
        add1.setSpam(0);
        add1.setImage(R.mipmap.project_gateway_logo);

        Banner add3 = new Banner();
        add1.setPosition(3);
        add1.setSpam(0);
        add1.setImage(R.mipmap.ic_launcher);


        Banner add4 = new Banner();
        add1.setPosition(4);
        add1.setSpam(1);
        add1.setImage(R.mipmap.agrochemicals);

        Banner add5 = new Banner();
        add1.setPosition(4);
        add1.setSpam(1);
        add1.setImage(R.mipmap.agrochemicals);


        addArrayList.add(add1);
        addArrayList.add(add2);
        addArrayList.add(add3);
        addArrayList.add(add5);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LinearLayout twoPArtLayout = new LinearLayout(ThirdActivity.this);
                twoPArtLayout.setOrientation(LinearLayout.HORIZONTAL);
                twoPArtLayout.setWeightSum(1);


                LinearLayout.LayoutParams childParam1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
                childParam1.weight = 1.0f;
                twoPArtLayout.setLayoutParams(childParam1);


                LinearLayout.LayoutParams imageParam1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                imageParam1.weight = 0.3f;


                LinearLayout.LayoutParams imageParam2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                imageParam1.weight = 0.7f;


                ImageView image = new ImageView(ThirdActivity.this);
                image.setLayoutParams(imageParam1);
                Bitmap Icon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.agrochemicals);
                image.setImageBitmap(Icon);
                image.setScaleType(ImageView.ScaleType.FIT_XY);

                ImageView image2 = new ImageView(ThirdActivity.this);
                image2.setLayoutParams(imageParam2);
                Bitmap Icon2 = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.agrochemicals);
                image2.setImageBitmap(Icon2);
                image2.setScaleType(ImageView.ScaleType.FIT_XY);



               /* ImageView image = new ImageView(ThirdActivity.this);
                image.setLayoutParams(new android.view.ViewGroup.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
              //  image.setScaleType(ImageView.ScaleType.FIT_XY);
*/

/*

                image.setMaxHeight(200);
                image.setMaxWidth(200);
*/



                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent i = new Intent(getApplicationContext(),SecondActivity.class);
                        startActivity(i);

                    }
                });

                twoPArtLayout.addView(image);
                twoPArtLayout.addView(image2);

                // Adds the view to the layout
                addContaintLayout.addView(twoPArtLayout);

            }
        });

    }



    class Banner {
        int wSize;
        int hSize;
        int position;
        int spam;
        int xLoc;
        int yLoc;
        int image;

        public int getImage() {
            return image;
        }

        public void setImage(int image) {
            this.image = image;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public int getxLoc() {
            return xLoc;
        }

        public void setxLoc(int xLoc) {
            this.xLoc = xLoc;
        }

        public int getyLoc() {
            return yLoc;
        }

        public void setyLoc(int yLoc) {
            this.yLoc = yLoc;
        }

        public int getwSize() {
            return wSize;
        }

        public void setwSize(int wSize) {
            this.wSize = wSize;
        }

        public int gethSize() {
            return hSize;
        }

        public void sethSize(int hSize) {
            this.hSize = hSize;
        }

        public int getSpam() {
            return spam;
        }

        public void setSpam(int spam) {
            this.spam = spam;
        }
    }



}
