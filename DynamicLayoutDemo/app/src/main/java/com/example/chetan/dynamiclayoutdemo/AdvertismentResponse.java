package com.example.chetan.dynamiclayoutdemo;

import java.util.ArrayList;

/**
 * Created by chetan on 17/8/15.
 */
public class AdvertismentResponse {

    private ArrayList<AddBanner> bannerRow = new ArrayList<AddBanner>();


    public ArrayList<AddBanner> getBannerRow() {
        return bannerRow;
    }

    public void setBannerRow(ArrayList<AddBanner> bannerRow) {
        this.bannerRow = bannerRow;
    }
}
