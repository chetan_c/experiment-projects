/*
package com.example.chetan.dynamiclayoutdemo;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;

import java.util.ArrayList;

import android.widget.TableRow.LayoutParams;


public class MainActivity extends ActionBarActivity {

    Button next;
    LinearLayout addContaintLayout=null;
    private ArrayList<AddBanner> bannerRow = new ArrayList<AddBanner>();
    private ArrayList<AddBanner.BannerDiamention> banners1 = new ArrayList<AddBanner.BannerDiamention>();
    private ArrayList<AddBanner.BannerDiamention> banners2 = new ArrayList<AddBanner.BannerDiamention>();
    private ArrayList<AddBanner.BannerDiamention> banners3 = new ArrayList<AddBanner.BannerDiamention>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addContaintLayout = (LinearLayout)findViewById(R.id.addContainLayout);


        AdvertismentResponse response = new AdvertismentResponse();

        AddBanner firstBanner = new AddBanner();
        AddBanner secondBanner = new AddBanner();
        AddBanner thirdBanner = new AddBanner();


        AddBanner.BannerDiamention b1Diamention = firstBanner.new BannerDiamention();
        b1Diamention.setCol("1");
        b1Diamention.setColspan("1");
        b1Diamention.setImgUrl("");
        b1Diamention.setUrl("");

        AddBanner.BannerDiamention b2Diamention = firstBanner.new BannerDiamention();
        b1Diamention.setCol("2");
        b1Diamention.setColspan("2");
        b1Diamention.setImgUrl("");
        b1Diamention.setUrl("");

        banners1.add(b1Diamention);

        banners2.add(b1Diamention);
        banners2.add(b2Diamention);



        banners3.add(b1Diamention);
        banners3.add(b1Diamention);
        banners3.add(b1Diamention);

        firstBanner.setHeight("300");
        firstBanner.setNumber("1");
        firstBanner.setBanners(banners1);

        secondBanner.setHeight("200");
        secondBanner.setNumber("2");
        secondBanner.setBanners(banners2);

        thirdBanner.setNumber("3");
        thirdBanner.setHeight("100");
        thirdBanner.setBanners(banners3);

        bannerRow.add(firstBanner);
        bannerRow.add(secondBanner);
        bannerRow.add(thirdBanner);

        response.setBannerRow(bannerRow);

        System.out.println("Advertisments object " + response.toString());
        Log.d("Advertisments object ", response.toString());


        displayAdvertisment(response);



    }

    private void displayAdvertisment(AdvertismentResponse response) {

         ArrayList<AddBanner> displaybannerRow = new ArrayList<AddBanner>();
        AddBanner firstBanner = null;

        int rowSize,bannerHight,number;

        if(response!=null)
        {
            displaybannerRow = response.getBannerRow();
            rowSize = displaybannerRow.size();

            System.out.println("Advertisments row Size " + rowSize);

            for(int i =0 ;i<displaybannerRow.size();i++)
            {
                 ArrayList<AddBanner.BannerDiamention> displayBanners = new ArrayList<AddBanner.BannerDiamention>();

                firstBanner = displaybannerRow.get(i);
                displayBanners = firstBanner.getBanners();
                bannerHight = Integer.parseInt(firstBanner.getHeight());
                number = Integer.parseInt(firstBanner.getNumber());

                System.out.println("Advertisments Display banner Size " + displayBanners.size());


                if (displayBanners.size()==1)
                {
                    ImageView image = new ImageView(MainActivity.this);

                    LinearLayout.LayoutParams imageParam1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                   // imageParam1.weight = 0.3f;

                    image.setPadding(5,5,5,5);
                    image.setLayoutParams(imageParam1);
                    Bitmap Icon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.agrochemicals);
                    image.setImageBitmap(Icon);
                    image.setScaleType(ImageView.ScaleType.FIT_XY);
                    image.setMaxHeight(bannerHight);

                    addContaintLayout.addView(image);

                }else
                {
                    LinearLayout twoPArtLayout = new LinearLayout(MainActivity.this);
                    twoPArtLayout.setOrientation(LinearLayout.HORIZONTAL);
                    twoPArtLayout.setWeightSum(displayBanners.size());

                    for (int j=0;j<displayBanners.size();j++)
                    {
                        ImageView image = new ImageView(MainActivity.this);

                        LinearLayout.LayoutParams imageParam1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                         imageParam1.weight = 1f;

                        image.setLayoutParams(imageParam1);
                        Bitmap Icon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.agrochemicals);
                        image.setImageBitmap(Icon);
                        image.setScaleType(ImageView.ScaleType.FIT_XY);
                        image.setPadding(5, 5, 5, 5);
                        image.setMaxHeight(bannerHight);
                        twoPArtLayout.addView(image);

                    }

                    addContaintLayout.addView(twoPArtLayout);

                }


            }


        }


    }

}
*/
