package com.example.chetan.dynamiclayoutdemo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chetan on 18/8/15.
 */
public class CreateAddDetails implements Parcelable {

    private String hight;
    private  String column;

    public CreateAddDetails()
    {

    }

    public String getHight() {
        return hight;
    }

    public void setHight(String hight) {
        this.hight = hight;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(hight);
        dest.writeString(column);
    }

    public static final Parcelable.Creator<CreateAddDetails> CREATOR = new Parcelable.Creator<CreateAddDetails>() {
        public CreateAddDetails createFromParcel(Parcel in) {
            return new CreateAddDetails(in);
        }

        public CreateAddDetails[] newArray(int size) {
            return new CreateAddDetails[size];
        }
    };



    // example constructor that takes a Parcel and gives you an object populated with it's values
    private CreateAddDetails(Parcel in) {
        hight = in.readString();
        column = in.readString();
    }
}
